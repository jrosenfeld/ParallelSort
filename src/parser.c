/*
 * parser.c
 *
 *  Created on: 13.03.2016
 *      Author: jrosenfeld
 */

#include "parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>

#include <string.h>
data* parse(FILE* file) {
	long int beginning = ftell(file);
	wchar_t nextChar = fgetwc(file);
	int count = 1;
	do {
		fseek(file, beginning, SEEK_SET);
		long int size = sizeOfNextObj(file);

		wchar_t* jsonobj = readNextObj(file, size);
		printf("%d : %ls with size (%d)\n", count, jsonobj,
				(sizeof(jsonobj) / sizeof(wchar_t)));
		count++;
		free(jsonobj);
		beginning = ftell(file);
		skipWhitespaces(file);
		nextChar = fgetwc(file);
	} while (!feof(file));
	fseek(file, 0L, SEEK_SET);
	long int* test = findObjectPositions(file);
	int sizeTest = sizeOfObjList(test);
	printf("You have found %d objects.\n", sizeTest);


	free(test);
	data* obj = (data*) malloc(sizeof(data));
	//obj->i=size;

	return obj;
}

FILE* openFile(char* path, char* flags) {
	FILE* file = fopen(path, flags);
	if (file == 0) {
		fprintf(stderr, "fopen(%s, %s) failed\n", path, flags);
		exit(6);
	}
	return file;
}

int closeFile(FILE* file) {
	return fclose(file);
}

long int sizeOfNextObj(FILE* file) {
	skipWhitespaces(file);
	long int beginning = ftell(file);
	long int size = seekNextObj(file) - beginning;
	fseek(file, beginning, SEEK_SET);
	return size;

}
long int seekNextObj(FILE* file) {
	long int beginning = ftell(file);
	skipWhitespaces(file);
	wchar_t firstChar = fgetwc(file);

	if (firstChar != L'{') {
		fprintf(stderr,
				"The json is not formed as expected at position %ld : %lc (%d).",
				beginning, firstChar, (int) firstChar);
		exit(7);
	}
	int brackets = 1;
	wchar_t nextChar;
	do {
		nextChar = fgetwc(file);
		if (nextChar == L'{') {
			brackets = brackets + 1;
		} else if (nextChar == L'}') {
			brackets = brackets - 1;
		}
	} while (brackets != 0 && !feof(file));
	if (brackets != 0) {
		fprintf(stderr,
				"The json is malformed. After position %ld there might be unclosed object.",
				beginning);
		exit(8);
	}
	long int ending = ftell(file);
	return ending + 1;
}
wchar_t* readNextObj(FILE* file, long int sizeOfNextObj) {
	wchar_t* obj = malloc(sizeOfNextObj * sizeof(wchar_t));
	if (fgetws(obj, sizeOfNextObj, file) == NULL) {
		fprintf(stderr, "Error occured reading next obj");
		exit(9);
	}
	return obj;
}
void skipWhitespaces(FILE* file) {
	long int lastWhitespace = ftell(file);
	wchar_t nextChar = fgetwc(file);
	if (!iswspace(nextChar)) {
		fseek(file, lastWhitespace, SEEK_SET);
		return;
	}
	skipWhitespaces(file);
}
long int* findObjectPositions(FILE* file) {
	skipWhitespaces(file);
	int amoutOfObject = 0;
	long int* beginnings = NULL;
	long int* tmp = NULL;
	long int beginning = ftell(file);
	wchar_t nextChar = fgetwc(file);
	do {
		fseek(file, beginning, SEEK_SET);

		tmp = (long int*) malloc((1 + amoutOfObject) * sizeof(long int)+1);
		if (beginnings) {

			memcpy(tmp, beginnings, (1 + amoutOfObject) * sizeof(long int));

			free(beginnings);
			beginnings = NULL;
		}

		*(tmp + amoutOfObject) = beginning;
		*(tmp+amoutOfObject+1) = -1;
		beginnings = tmp;

		beginning = seekNextObj(file);

//		printf("The %d. beginnig is at %ld\n", amoutOfObject,
//				*(beginnings + amoutOfObject));
		amoutOfObject = amoutOfObject + 1;
		skipWhitespaces(file);
		beginning = ftell(file);

		nextChar = fgetwc(file);
	} while (!feof(file));


	return beginnings;
}
int sizeOfObjList(long int* arr){
	int pos = 0;
	while(1){
		long int curr = *(arr+pos);
		if (curr<0){
			break;
		}
		pos++;
	}

	return pos;
}

