/*
 * parser.h
 *
 *  Created on: 13.03.2016
 *      Author: jrosenfeld
 */
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#ifndef PARSER_H_
#define PARSER_H_

typedef struct  {
 char* id;
 wchar_t* text;
} data;

data* parse(FILE* file);
FILE* openFile(char* path, char* flags);
int closeFile(FILE* file);
long int sizeOfNextObj(FILE* file);
long int seekNextObj(FILE* file);
wchar_t* readNextObj(FILE* file, long int sizeOfNextObj);
void skipWhitespaces(FILE* file);
long int* findObjectPositions(FILE* file);
int sizeOfObjList(long int* arr);
#endif /* PARSER_H_ */
