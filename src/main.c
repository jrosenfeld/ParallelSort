#include <stdio.h>
#include <stdlib.h>
#include "parser.h"

int main() {
	FILE* file = openFile("/home/jrosenfeld/twitter_json.json", "r");
	data* obj = parse(file);
	closeFile(file);
	free(obj);
	return 0;
}